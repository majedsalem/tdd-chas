
def getInput(inputPath):
    numbers = []
    with open(inputPath) as f:
        lines = f.read().splitlines()
        numbers = lines

    return numbers

def getMeasurement(newNumber, oldNumber):
    if(newNumber > oldNumber):
        return True
    else:
        return False

def getResultString(result):
    if(result):
        return "(increased)"
    else:
        return "(decreased)"

def loopInInputs(numbers):
    print(numbers)
    resultList = []
    for number in range(len(numbers)):
        if(number == 0):
            resultList.append(numbers[number] + " " + "(N/A - no previous measurement)")
        else:
            result = getMeasurement(int(numbers[number]), int(numbers[number - 1]))
            resultList.append(numbers[number] + " " + getResultString(result))
    return resultList
#Day 2

def getInputCommands(inputCommandPath):
    number = []
    with open(inputCommandPath) as f:
        lines = f.read().splitlines()
        number = lines

    return number

def calculateMoving(commands):
    horizontal = 0
    depth = 0
    print(commands)
    for command in commands:
        order = command.split()[0]
        position = command.split()[1]
        if(order == "forward"):
            horizontal = horizontal + int(position)
        elif(order == "down"):
            depth = depth +  int(position)
        elif(order == "up"):
            depth = depth - int(position)

    return [horizontal, depth]


def main():
    numbers = getInput("input.txt")
    loopInInputs(numbers)
    commands = getInputCommands("commands.txt")
    print(calculateMoving(commands))

if __name__ == "__main__":
    main()

