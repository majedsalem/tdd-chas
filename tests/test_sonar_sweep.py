import unittest;

from main import getInput, getMeasurement, getResultString, loopInInputs, getInputCommands, calculateMoving


class TestAoC(unittest.TestCase):

    def test_measurement_get_input_data_returns_int_array(self):
        input = getInput("input.txt")
        self.assert_(input, [])

    def test_measurement_find_measurement_result_return_false(self):
        result = getMeasurement(10, 20)
        print(result)
        self.assertFalse(result, False)

    def test_measurement_find_measurement_result_return_true(self):
        result = getMeasurement(20, 10)
        self.assertTrue(result, True)

    def test_measurement_find_measurement_result_return_string_increases(self):
        increases = getResultString(True)
        self.assert_(increases, "(increases)")

    def test_measurement_find_measurement_result_return_string_decreased(self):
        increases = getResultString(True)
        self.assert_(increases, "(decreased)")

    def test_measurement_loop_in_inputs(self):
        inputs = ['122', '33', '123', '44', '545']
        data = loopInInputs(inputs)
        self.assertIn(str(data[0]), '122 (N/A - no previous measurement)')
        self.assertIn(str(data[1]), '33 (decreased)')
        self.assertIn(str(data[2]), '123 (increased)')


    def test_commands_moving_inputs_read(self):
        input = getInputCommands("commands.txt")
        self.assert_(input, [])

    def test_commands_moving_forward(self):
        commands = ['forward 9', 'down 9', 'up 4', 'down 5', 'down 6', 'up 6', 'down 7', 'down 1', 'forward 6']
        unit = calculateMoving(commands)
        self.assert_(unit[0], 15)
        self.assert_(unit[1], 18)
